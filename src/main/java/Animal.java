/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public abstract class Animal {
    private String name;
    private  int numberOfleg;

    public Animal(String name, int numberOfleg) {
        this.name = name;
        this.numberOfleg = numberOfleg;
    }

    public String getName() {
        return name;
    }

    public int getNumberOfleg() {
        return numberOfleg;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNumberOfleg(int numberOfleg) {
        this.numberOfleg = numberOfleg;
    }
    public abstract void eat();
    public abstract void speak();
    public abstract  void sleep();
    
}
